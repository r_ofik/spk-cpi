@extends('layouts.app', [
    'title' => 'Detail Masyarakat', 
    'active'    => 'masyarakat'
])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header border-bottom">
                    <div class="pull-right">
                        <a href="{{ route('masyarakat.index') }}" class="btn btn-info m-0" >Kembali</a>
                    </div>
                    <h5>{{ $masyarakat->nama }}</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">NIK</label>
                                <input type="text" class="form-control-plaintext" value="{{ $masyarakat->nik }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nama Lengkap</label>
                                <input type="text" class="form-control-plaintext" value="{{ $masyarakat->nama }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Tempat, Tanggal Lahir</label>
                                <input type="text" class="form-control-plaintext" value="{{ $masyarakat->tmp_lahir.', '.date_format(date_create($masyarakat->tgl_lahir), 'd-m-Y') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <input type="text" class="form-control-plaintext" value="{{ ($masyarakat->gender == 'L') ? 'Laki - Laki' : 'Perempuan' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Alamat</label>
                                <input type="text" class="form-control-plaintext" value="{{ $masyarakat->alamat }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="text" class="form-control-plaintext" value="{{ $masyarakat->keterangan }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('masyarakat.edit', $masyarakat->id) }}" class="btn btn-warning"><i class="nc-icon nc-ruler-pencil"></i> Edit</a>
                </div>
            </div>
        </div>
    </div>
@endsection