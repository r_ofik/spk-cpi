@extends('layouts.app', [
    'title' => 'Masyarakat', 
    'active'    => 'masyarakat'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header border-bottom">
                <div class="pull-right">
                    <button class="btn btn-success m-0" data-toggle="modal" data-target="#addModal" ><i class="nc-icon nc-simple-add"></i> Tambah</button>
                </div>
                <h5>Data Masyarakat</h5>
            </div>

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>TTL</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($masyarakats as $key => $masyarakat)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $masyarakat->nik }}</td>
                                <td>{{ $masyarakat->nama }}</td>
                                <td>{{ $masyarakat->alamat }}</td>
                                <td>{{ $masyarakat->tmp_lahir.', '.date_format(date_create($masyarakat->tgl_lahir), 'd-m-Y') }}</td>
                                <td>{{ $masyarakat->keterangan }}</td>
                                <td>
                                    <a href="{{ route('masyarakat.detail', $masyarakat->id) }}" class="btn btn-sm btn-info"><i class="nc-icon nc-zoom-split"></i></a>
                                    <a href="{{ route('masyarakat.edit', $masyarakat->id) }}" class="btn btn-warning btn-sm"><i class="nc-icon nc-ruler-pencil"></i></a>
                                    <button onclick="confirmDelete({{ $masyarakat->id }})" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addLabel">Tambah Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="modal-body">
            <div class="form-group">
                <label for="">NIK <span class="text-danger">*</span></label>
                <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror" placeholder="NIK" value="{{ old('nik') }}">
                @error('nik')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Nama Lengkap <span class="text-danger">*</span></label>
                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Lengkap" value="{{ old('nama') }}">
                @error('nama')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Tempat, Tanggal Lahir</label>
                <div class="row">
                    <div class="col-md-7">
                        <input type="text" name="tmp_lahir" class="form-control @error('tmp_lahir') is-invalid @enderror" placeholder="tempat Lahir" value="{{ old('tmp_lahir') }}">
                        @error('tmp_lahir')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-5">
                        <input type="date" name="tgl_lahir" class="form-control @error('tgl_lahir') is-invalid @enderror" value="{{ old('tgl_lahir') }}">
                        @error('tgl_lahir')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Jenis Kelamin <span class="text-danger">*</span></label>
                <select name="jenis_kelamin" class="custom-select @error('jenis_kelamin') is-invalid @enderror">
                    <option value="">-- Pilih --</option>
                    <option value="L" @if(old('jenis_kelamin') == 'L') selected @endif>Laki-Laki</option>
                    <option value="P" @if(old('jenis_kelamin') == 'P') selected @endif>Perempuan</option>
                </select>
                @error('jenis_kelamin')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Alamat</label>
                <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" placeholder="Alamat" value="{{ old('alamat') }}">
                @error('alamat')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Keterangan</label>
                <input type="text" name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan" value="{{ old('keterangan') }}">
                @error('keterangan')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
      </div>
    </div>
  </div>

<form action="" method="post" id="delete">
    @method('DELETE')
    @csrf
</form>
@endsection
@push('script')
<script src="{{asset('assets/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
function confirmDelete(id) {
    Swal.fire({
          title: "Apa anda yakin?",
          text: "Anda akan kehilangan data ini!",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Ya, Hapus!",
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn btn-danger",
          cancelButtonClass: "btn btn-primary ml-1",
          buttonsStyling: false
        }).then(function(result) {
          if (result.value) {
            $('#delete').attr('action', '/masyarakat/'+id);
            $('#delete').submit();
          }
        });
}
</script>
@if (session('status'))
<script>
    $.notify({
            icon: "nc-icon nc-bell-55",
            message: "{{ session('status') }}"

        }, {
            type: 'success',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
</script>
@endif
@if ($errors->any())
    <script>
        $('#addModal').modal('show');
    </script>
@endif
@endpush
