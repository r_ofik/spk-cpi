@extends('layouts.app', [
    'title' => 'Goal Data', 
    'active'    => 'goal'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header border-bottom">
                <h5>Goal Data</h5>
            </div>

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Nilai CPI</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($masyarakats as $key => $masyarakat)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $masyarakat->nik }}</td>
                                <td>{{ $masyarakat->nama }}</td>
                                <td>{{ $masyarakat->cpi }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection