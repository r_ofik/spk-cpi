@extends('layouts.app', [
    'title' => 'Kriteria', 
    'active'    => 'kriteria'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header border-bottom">
                <div class="pull-right">
                    <button class="btn btn-success m-0" data-toggle="modal" data-target="#addModal" ><i class="nc-icon nc-simple-add"></i> Tambah</button>
                </div>
                <h5>Data Kriteria</h5>
            </div>

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Bobot</th>
                            <th>Tren</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($criterias as $key => $criteria)
                            <tr>
                                <td>C{{ $key+1 }}</td>
                                <td>{{ $criteria->name }}</td>
                                <td>{{ $criteria->bobot }}</td>
                                <td>{{ ($criteria->tren == 'positive') ? 'Positif' : 'Negatif' }}</td>
                                <td>
                                    <button onclick="update({{ $criteria }})"  class="btn btn-warning btn-sm"><i class="nc-icon nc-ruler-pencil"></i></button>
                                    <button onclick="confirmDelete({{ $criteria->id }})" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addLabel">Tambah Kriteria</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="modal-body">
            <div class="form-group">
                <label for="">Nama <span class="text-danger">*</span></label>
                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Kriteria" value="{{ old('nama') }}">
                @error('nama')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group row">
                    
                <div class="col-md-6">
                    <label for="">Bobot <span class="text-danger">*</span></label>
                    <input type="text" name="bobot" class="form-control @error('bobot') is-invalid @enderror" placeholder="Bobot" value="{{ old('bobot') }}">
                    @error('bobot')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="">Tren <span class="text-danger">*</span></label>
                    <select name="tren" class="custom-select @error('tren') is-invalid @enderror">
                        <option value="">-- Pilih --</option>
                        <option value="positive" @if(old('tren') == 'positive') selected @endif>Positif</option>
                        <option value="negative" @if(old('tren') == 'negative') selected @endif>Negatif</option>
                    </select>
                    @error('tren')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editLabel">Edit Kriteria</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="POST" id="edit">
            @method('PUT')
            @csrf
            <div class="modal-body">
            <div class="form-group">
                <label for="">Nama <span class="text-danger">*</span></label>
                <input type="text" id="nama" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Kriteria" value="{{ old('nama') }}">
                @error('nama')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group row">
                    
                <div class="col-md-6">
                    <label for="">Bobot <span class="text-danger">*</span></label>
                    <input type="text" id="bobot" name="bobot" class="form-control @error('bobot') is-invalid @enderror" placeholder="Bobot" value="{{ old('bobot') }}">
                    @error('bobot')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="">Tren <span class="text-danger">*</span></label>
                    <select name="tren" id="tren" class="custom-select @error('tren') is-invalid @enderror">
                        <option value="">-- Pilih --</option>
                        <option value="positive" @if(old('tren') == 'positive') selected @endif>Positif</option>
                        <option value="negative" @if(old('tren') == 'negative') selected @endif>Negatif</option>
                    </select>
                    @error('tren')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>

<form action="" method="post" id="delete">
    @method('DELETE')
    @csrf
</form>
@endsection
@push('script')
<script src="{{asset('assets/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
    function confirmDelete(id) {
        Swal.fire({
            title: "Apa anda yakin?",
            text: "Anda akan kehilangan data ini!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak",
            confirmButtonClass: "btn btn-danger",
            cancelButtonClass: "btn btn-primary ml-1",
            buttonsStyling: false
            }).then(function(result) {
            if (result.value) {
                $('#delete').attr('action', '/kriteria/'+id);
                $('#delete').submit();
            }
            });
    }

    //Update Modal
    function update(data){
        $('#edit').attr('action', '/kriteria/'+data.id)
        $('#edit #nama').val(data.name)
        $('#edit #bobot').val(data.bobot)
        $('#edit #tren').val(data.tren).change()

        $('#editModal').modal('show')
    }
</script>
@if (session('status'))
<script>
    $.notify({
            icon: "nc-icon nc-bell-55",
            message: "{{ session('status') }}"

        }, {
            type: 'success',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
</script>
@endif
@if ($errors->any())
    <script>
        $('#addModal').modal('show');
    </script>
@endif
@endpush
