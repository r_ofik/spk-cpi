@extends('layouts.app', [
    'title' => 'Alternatif', 
    'active'    => 'alternatif'
])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header border-bottom">
                    <div class="pull-right">
                        <button class="btn btn-success m-0" data-toggle="modal" data-target="#addModal" ><i class="nc-icon nc-simple-add"></i> Tambah</button>
                    </div>
                    <h5>
                        Data Alternatif
                    </h5>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th rowspan="2" class="text-center border">#</th>
                                <th rowspan="2" class="border">Alternatif</th>
                                <th colspan="{{ count($criterias) }}" class="text-center border-bottom border">Kriteria</th>
                                <th rowspan="2" class="text-center border">Aksi</th>
                            </tr>
                            <tr>
                                @foreach ($criterias as $k => $criteria)
                                    <th class="text-center border">{{ $criteria->name }} (C{{ $k+1 }})</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alternatives as $key => $alternative)
                                <tr>
                                    <td class="text-center">A{{ $key+1 }}</td>
                                    <td>{{ $alternative->nama }}</td>
                                    @foreach ($alternative->criterias as $ac)
                                        <td class="text-center">{{ $ac->pivot->nilai }}</td>
                                    @endforeach
                                    <td class="text-center">
                                        <button onclick="confirmDelete({{ $alternative->id }})" class="btn btn-danger btn-sm"><i class="nc-icon nc-simple-remove"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<!-- Modal Add -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addLabel">Tambah Alternatif</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Pilih Data</label>
                    <select name="data" class="custom-select @error('data') is-invalid @enderror">
                        <option value="">-- Pilih --</option>
                        @foreach ($data as $item)
                            <option value="{{ $item->id }}" @if(old('data') == $item->id) selected @endif>{{ $item->nama }}</option>
                        @endforeach
                    </select>
                    @error('data')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <hr>
                <div class="row">
                    @foreach ($criterias as $key => $criteria)
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">{{ $criteria->name }}</label>
                                <input type="hidden" name="criteria[{{ $key }}][id]" class="form-control" value="{{ $criteria->id }}">
                                <input type="text" name="criteria[{{$key}}][value]" class="form-control" placeholder="Nilah {{ $criteria->name }}">
                            </div>
                        </div>
                    @endforeach
                </div>
                @if ($errors->any()) 
                    <div class="invalid-feedback" style="display: block">Harap Lengkapi Data.</div>
                @endif
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>

<form action="" method="post" id="delete">
    @method('DELETE')
    @csrf
</form>
@endsection

@push('script')
<script src="{{asset('assets/js/plugins/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/sweet-alerts.js')}}"></script>
<script>
    //Delete Confirmation
    function confirmDelete(id) {
        Swal.fire({
            title: "Apa anda yakin?",
            text: "Anda akan kehilangan data ini!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak",
            confirmButtonClass: "btn btn-danger",
            cancelButtonClass: "btn btn-primary ml-1",
            buttonsStyling: false
            }).then(function(result) {
            if (result.value) {
                $('#delete').attr('action', '/alternatif/'+id);
                $('#delete').submit();
            }
            });
    }
</script>
@if (session('status'))
<script>
    $.notify({
            icon: "nc-icon nc-bell-55",
            message: "{{ session('status') }}"

        }, {
            type: 'success',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
</script>
@endif

@if (session('error'))
<script>
    $.notify({
            icon: "nc-icon nc-bell-55",
            message: "{{ session('error') }}"

        }, {
            type: 'warning',
            timer: 1000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
</script>
@endif

@if ($errors->any())
    <script>
        $('#addModal').modal('show');
    </script>
@endif
@endpush