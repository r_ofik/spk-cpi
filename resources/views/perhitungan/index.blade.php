@extends('layouts.app', [
    'title' => 'Proses Perhitungan', 
    'active'    => 'perhitungan'
])

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header border-bottom">
                <h5>Proses Perhitungan Metode CPI</h5>
            </div>

            <div class="card-body">
                {{-- Matrix Perbandingan --}}
                <div class="card">
                    <div class="card-header border-bottom">Matrik Perbandingan</div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-light">
                                <tr>
                                    <th rowspan="2" class="border text-center">Alternatif</th>
                                    <th colspan="{{ count($criterias) }}" class="text-center border-bottom border">Kriteria</th>
                                </tr>
                                <tr>
                                    @foreach ($criterias as $k => $criteria)
                                        <th class="text-center border"><span data-toggle="tooltip" data-placement="top" title="{{ $criteria->name }}">C{{ $k+1 }}</span></th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alternatives as $key => $alternative)
                                    <tr>
                                        <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $alternative->nama }}">A{{ $key+1 }}</span></td>
                                        @foreach ($alternative->criterias as $ac)
                                            <td class="text-center">{{ $ac->pivot->nilai }}</td>
                                            @php
                                                $ab[$ac->id][] = $ac->pivot->nilai;
                                            @endphp
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center">Nilai Min</th>
                                    @foreach ($ab as $a)
                                        <th class="text-center">{{ min($a) }}</th>
                                    @endforeach
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                {{-- Bobot Kepentingan dan Tren --}}
                <div class="card">
                    <div class="card-header border-bottom">Bobot Kepentingan (P) dan Tren-nya</div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-light">
                                <tr>
                                    <th>Kriteria</th>
                                    @foreach ($criterias as $key => $criteria)
                                        <th>{{ $criteria->name }} (C{{ $key+1 }})</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Bobot</th>
                                    @foreach ($criterias as $key => $criteria)
                                        <td>{{ $criteria->bobot }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Tren</th>
                                    @foreach ($criterias as $key => $criteria)
                                        <td>{{ Str::ucfirst($criteria->tren) }}</td>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                {{-- Perhitungan --}}
                @foreach ($criterias as $key => $criteria)
                <div class="card">
                    <div class="card-header border-bottom">Table Perhitungan Nilai {{ $criteria->name }} ({{ $criteria->tren == 'positive' ? '+' : '-'}}) <br>
                        {{ ($criteria->tren == 'positive') ? 'Tren (+) = Nilai N / Nilai Min * 100' : 'Tren (-) = Nilai Min/ Nilai N * 100'}}
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-light">
                                <tr>
                                    <th>Alternatif</th>
                                    <th>{{ $criteria->name }}</th>
                                    <th>{{ $criteria->tren == 'positive' ? 'N/Min' : 'Min/N'}}</th>
                                    <th>{{ $criteria->tren == 'positive' ? 'N/Min*100' : 'Min/N*100'}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($criteria->alternatives as $k => $alternative)
                                    <tr>
                                        <td>{{ $alternative->nama }}</td>
                                        <td>{{ $alternative->pivot->nilai }}</td>
                                        <td>{{ ($criteria->tren == 'positive') ? $alternative->pivot->nilai/$alternative->pivot->n_min : $alternative->pivot->n_min/$alternative->pivot->nilai }}</td>
                                        <td>{{ $alternative->pivot->n_tren }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endpush
