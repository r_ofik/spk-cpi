<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register'  => false
]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Routing for Masyarakat Menu
Route::get('/masyarakat', [App\Http\Controllers\MasyarakatController::class, 'index'])->name('masyarakat.index');
Route::post('/masyarakat', [App\Http\Controllers\MasyarakatController::class, 'store'])->name('masyarakat.store');
Route::get('/masyarakat/{id}', [App\Http\Controllers\MasyarakatController::class, 'show'])->name('masyarakat.detail');
Route::get('/masyarakat/{id}/edit', [App\Http\Controllers\MasyarakatController::class, 'edit'])->name('masyarakat.edit');
Route::put('/masyarakat/{id}', [App\Http\Controllers\MasyarakatController::class, 'update'])->name('masyarakat.update');
Route::delete('/masyarakat/{id}', [App\Http\Controllers\MasyarakatController::class, 'destroy'])->name('masyarakat.destroy');

//Routing for Kriteria Menu
Route::get('/kriteria', [App\Http\Controllers\CriteriaController::class, 'index'])->name('kriteria.index');
Route::post('/kriteria', [App\Http\Controllers\CriteriaController::class, 'store'])->name('kriteria.store');
Route::put('/kriteria/{id}', [App\Http\Controllers\CriteriaController::class, 'update'])->name('kriteria.update');
Route::delete('/kriteria/{id}', [App\Http\Controllers\CriteriaController::class, 'destroy'])->name('kriteria.destroy');

//Routing Alternatif Menu
Route::get('/alternatif', [App\Http\Controllers\AlternativeController::class, 'index'])->name('alternatif.index');
Route::post('/alternatif', [App\Http\Controllers\AlternativeController::class, 'store'])->name('alternatif.store');
Route::delete('/alternatif/{id}', [App\Http\Controllers\AlternativeController::class, 'destroy'])->name('alternatif.destroy');
Route::get('/goal', [App\Http\Controllers\MasyarakatController::class, 'goal'])->name('goal.index');

Route::get('/perhitungan', [App\Http\Controllers\PerhitunganController::class, 'index'])->name('perhitungan.index');

Route::get('/profil', [App\Http\Controllers\ProfileController::class, 'index'])->name('profil.index');
Route::put('/profil', [App\Http\Controllers\ProfileController::class, 'update'])->name('profil.update');
Route::post('/profil', [App\Http\Controllers\ProfileController::class, 'updatePassword'])->name('password.update');

