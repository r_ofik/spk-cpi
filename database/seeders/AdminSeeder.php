<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['id' => 1],[
            'name'  => 'Administrator',
            'email' => 'admin@me.com',
            'password'  => \Hash::make('admin123')
        ]);
    }
}
