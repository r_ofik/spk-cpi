<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alternative;
use App\Models\AlternativeCriteria as AC;
use App\Models\Criteria;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ac = AC::get();
        $alt = [];
        foreach ($ac as $value) {
            $alt[] = $value->alternative_id;
        }

        // $data['alternatives'] = Alternative::whereIn('id', $alt)->with('criterias')->latest()->get();
        $data['jml_alternative'] = Alternative::whereIn('id', $alt)->count();
        $data['jml_kriteria'] = Criteria::count();
        $data['jml_masyarakat'] = Alternative::count();

        $data['alternatives'] = Alternative::where('cpi', '!=', '0')->orderBy('cpi', 'DESC')->get();
        return view('home', $data);
    }
}
