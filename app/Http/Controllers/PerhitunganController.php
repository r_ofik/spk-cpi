<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\AlternativeCriteria as AC;

class PerhitunganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Perbandingan Matrik
        $ac = AC::get();
        $alt = [];
        foreach ($ac as $value) {
            $alt[] = $value->alternative_id;
        }

        $data['alternatives'] = Alternative::whereIn('id', $alt)->with('criterias')->latest()->get();
        $data['criterias'] = Criteria::with('alternatives')->get();
        //mengambil id dan nilai terkecil kriteria
        $nilai = [];
        foreach ($data['criterias'] as $criteria) {
            $nilai[] = [
                'c_id' => $criteria->id,
                'n_min' => AC::where('criteria_id', $criteria->id)->min('nilai'),
                'tren'  => $criteria->tren
            ];
        }
        //set nilai terkecil
        foreach ($nilai as $n) {
            AC::where('criteria_id', $n['c_id'])->update([
                'n_min' => $n['n_min']
            ]);
        }

        // get tren
        $n_tren = [];
        foreach ($ac as $n_a) {
            $n_tren[] = [
                'c_id'  => $n_a->id,
                'n_tren' => (Criteria::where('id', $n_a->criteria_id)->first()->tren == 'negative') ? ($n_a->n_min / $n_a->nilai) * 100 : ($n_a->nilai / $n_a->n_min) * 100,
                'tren'  => Criteria::where('id', $n_a->criteria_id)->first()->tren
            ];
        }

        //set tren
        foreach ($n_tren as $nt) {
            AC::where('id', $nt['c_id'])->update([
                'n_tren'    => $nt['n_tren'],
                'keterangan'=> $nt['tren']
            ]);
        }
        
        //get tren by altenarive and Calculate CPI
        $alt_id = array_unique($alt);
        $crit = [];
        foreach ($alt_id as $altid) {
            Alternative::where('id', $altid)->update([
                'cpi'   => $this->calCPI(AC::where('alternative_id', $altid)->get())
            ]);
        }

        return view('perhitungan.index', $data);
    }

    public function calCPI($data)
    {
        $cpi = 0;
        foreach ($data as $key => $value) {
            $cpi += $value->n_tren*Criteria::where('id', $value->criteria_id)->first()->bobot;
        }
        return $cpi;
    }
}
