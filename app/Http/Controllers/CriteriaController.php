<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Criteria;
use App\Models\AlternativeCriteria as AC;

class CriteriaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Menampilkan data kriteria
     */
    public function index()
    {
        $data['criterias'] = Criteria::latest()->get();

        return view('criteria.index', $data);
    }

    /**
     * Proses Tambah Kriteria
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'      => 'required',
            'bobot'     => 'required',
            'tren'      => 'required'  
        ]);

        Criteria::create([
            'name'  => $request->nama,
            'bobot' => $request->bobot,
            'tren'  => $request->tren
        ]);

        return redirect('kriteria')->with('status', 'Data telah disimpan!');
    }

    /**
     * Proses Update kriteria
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'      => 'required',
            'bobot'     => 'required',
            'tren'      => 'required'  
        ]);

        Criteria::where('id', $id)->update([
            'name'  => $request->nama,
            'bobot' => $request->bobot,
            'tren'  => $request->tren
        ]);

        return redirect('kriteria')->with('status', 'Data telah diubah!');
    }

    /**
     * Proses Hapus Kriteria
     */
    public function destroy($id)
    {
        Criteria::where('id', $id)->delete();

        AC::where('criteria_id', $id)->delete();

        return redirect('kriteria')->with('status', 'Data telah dihapus!');
    }
}
