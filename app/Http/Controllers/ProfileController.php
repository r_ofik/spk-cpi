<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['profile'] = Auth::user();

        return view('profile', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required'
        ]);

        User::where('id', Auth::user()->id)->update([
            'name'  => $request->name,
            'email' => $request->email
        ]);

        return redirect('profil')->with('status', 'Profil Berhasil Diubah!');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password'  => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        User::where('id', Auth::user()->id)->update([
            'password'  => \Hash::make($request->password),
        ]);

        return redirect('profil')->with('status', 'Password Berhasil Diubah!');
    }
}
