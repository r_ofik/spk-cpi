<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alternative as Masyarakat;
use App\Models\AlternativeCriteria as AC;

class MasyarakatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Menampilkan data Masyarakat
     */
    public function index()
    {
        $data['masyarakats'] = Masyarakat::latest()->get();

        return view('masyarakat.index', $data);
    }

    /**
     * Proses Simpan data ke database
     */
    public function store(Request $request)
    {
        //Validasi Form Tambah Data Masyarakat
        $request->validate([
            'nik'   => 'required|numeric|unique:alternatives',
            'nama'  => 'required',
            'jenis_kelamin' => 'required',
        ]);

        //Memasukan data ke database
        Masyarakat::create([
            'nik'       => $request->nik,
            'nama'      => $request->nama,
            'alamat'    => $request->alamat,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'gender'    => $request->jenis_kelamin,
            'cpi'       => '0',
            'keterangan'=> $request->keterangan,
        ]);

        return redirect('masyarakat')->with('status', 'Data telah disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['masyarakat'] = Masyarakat::where('id', $id)->first();

        return view('masyarakat.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['masyarakat'] = Masyarakat::where('id', $id)->first();

        return view('masyarakat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nik'   => 'required|numeric|unique:alternatives,nik,'.$id,
            'nama'  => 'required',
            'jenis_kelamin' => 'required'
        ]);

        Masyarakat::where('id', $id)->update([
            'nik'       => $request->nik,
            'nama'      => $request->nama,
            'alamat'    => $request->alamat,
            'tmp_lahir' => $request->tmp_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'gender'    => $request->jenis_kelamin,
            'keterangan'=> $request->keterangan,
        ]);

        return redirect('masyarakat')->with('status', 'Data telah diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Masyarakat::where('id', $id)->delete();

        AC::where('alternative_id', $id)->delete();

        return redirect('masyarakat')->with('status', 'Data telah dihapus!');
    }

    public function goal()
    {
        $data['masyarakats'] = Masyarakat::where('cpi', '!=', '0')->orderBy('cpi', 'DESC')->get();

        return view('goal.index', $data);
    }
}
