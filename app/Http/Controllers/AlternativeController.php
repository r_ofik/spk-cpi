<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\AlternativeCriteria as AC;

class AlternativeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Menampilkan data alaternatif
     */
    public function index()
    {
        $ac = AC::get();
        $alt = [];
        foreach ($ac as $value) {
            $alt[] = $value->alternative_id;
        }

        $data['alternatives'] = Alternative::whereIn('id', $alt)->with('criterias')->latest()->get();
        $data['criterias'] = Criteria::with('alternatives')->get();
        $data['data'] = Alternative::whereNotIn('id', $alt)->latest()->get();

        return view('alternative.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'data'  => 'required',
            'criteria.*.value'    => 'required'
        ]);

        if ($request->criteria == null) {
            return redirect('alternatif')->with('error', 'Data Kriteria Masih Kosong!');
        }

        foreach ($request->criteria as $cr) {
            AC::create([
                'criteria_id' => $cr['id'],
                'alternative_id'    => $request->data,
                'nilai'     => $cr['value']
            ]);
        }

        return redirect('alternatif')->with('status', 'Data telah disimpan!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AC::where('alternative_id', $id)->delete();

        return redirect('alternatif')->with('status', 'Data telah dihapus!');
    }
}
