<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function alternatives()
    {
        return $this->belongsToMany(Alternative::class)->withPivot('nilai', 'n_min', 'n_tren', 'keterangan');
    }
}
