<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    use HasFactory;

    protected $table = 'alternatives';

    protected $guarded = ['id'];

    public function criterias()
    {
        return $this->belongsToMany(Criteria::class)->withPivot('nilai', 'n_min', 'n_tren', 'keterangan');
    }
}
